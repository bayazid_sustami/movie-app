# Movie-App

Movie-App is an Android application built to provide users with a seamless experience in exploring and discovering movies based on various genres. It utilizes modern Android development tools and libraries including Jetpack Compose for UI, Retrofit for networking, Coroutine Flow for asynchronous operations, and more.

## Features

- Show Official Genre Movie: Browse through a curated list of official genre movies.
- Show Movie by Genre: show list movies based on specific genres.
- Show Detail Movie: View detailed information about each movie including its plot, ratings, etc.
- Show List Reviews of Movie: Read user reviews and ratings for each movie.
- Show Official Trailer: Watch trailers for selected movies.

## Technologies Used

- **Jetpack Compose**: Modern UI toolkit for building native Android UIs.
- **Retrofit**: Type-safe HTTP client for Android and Java.
- **Coroutine Flow**: Asynchronous programming library for managing background tasks.
- **Coil**: Image loading library for displaying movie posters and thumbnails.
- **Android Architecture Components**: Utilized for building robust and maintainable architecture.
- **Material Design Components**: UI components following the Material Design guidelines for a polished UI.

## Minimum Requirement
- JDK : 17.0.7
- Kotlin : 1.9.0
- Android Gradle Plugin : 8.1

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/bayazid_sustami/movie-app.git`
2. Open the project in Android Studio.
3. Build and run the project on your Android device or emulator.

## Screenshots

<img src="/previews/homepage.gif" width="320" />
<img src="/previews/detail.gif" width="320" />
<img src="/previews/trailer.gif" width="320" />
