package com.example.movieapp.domain.mapper

import com.example.movieapp.data.datasource.response.DetailMovieResponse
import com.example.movieapp.data.datasource.response.GenresResponse
import com.example.movieapp.data.datasource.response.MovieVideosResponse
import com.example.movieapp.data.datasource.response.ReviewsMovieResponse
import com.example.movieapp.domain.model.DetailMovieModelDomain
import com.example.movieapp.domain.model.MovieVideoModelDomain
import com.example.movieapp.domain.model.ReviewsMovieModelDomain
import javax.inject.Inject

class DetailMapper @Inject constructor(
    private val homeMapper: HomeMapper
) {

    fun mapResponseDetailToDomain(
        detailResponse: DetailMovieResponse,
        reviewsMovieResponse: ReviewsMovieResponse,
        videosResponse: MovieVideosResponse
    ): DetailMovieModelDomain {
        return DetailMovieModelDomain(
            id = detailResponse.id ?: 0,
            video = detailResponse.video ?: false,
            title = detailResponse.title ?: "",
            genres = homeMapper.mapGenreResponseToDomain(GenresResponse(detailResponse.genres)),
            popularity = detailResponse.popularity ?: 0.0,
            voteCount = detailResponse.voteCount ?: 0,
            voteAverage = detailResponse.voteAverage ?: 0.0,
            reviews = mapReviewsToDomain(reviewsMovieResponse),
            overview = detailResponse.overview ?: "",
            posterPath = detailResponse.posterPath ?: "",
            videos = mapVideosToDomain(videosResponse)
        )
    }

    fun mapReviewsToDomain(
        reviewsMovieResponse: ReviewsMovieResponse
    ): ReviewsMovieModelDomain {
        return ReviewsMovieModelDomain(
            page = reviewsMovieResponse.page ?: 0,
            totalResults = reviewsMovieResponse.totalResults ?: 0,
            totalPages = reviewsMovieResponse.totalPages ?: 0,
            id = reviewsMovieResponse.id ?: 0,
            results = reviewsMovieResponse.results.map {
                ReviewsMovieModelDomain.ReviewItem(
                    author = it.author ?: "",
                    createdAt = it.createdAt ?: "",
                    id = it.id ?: "0",
                    content = it.content ?: "",
                    url = it.url ?: ""
                )
            }
        )
    }

    fun mapVideosToDomain(
        videosResponse: MovieVideosResponse
    ): List<MovieVideoModelDomain> {
        return videosResponse.results.map {
            MovieVideoModelDomain(
                site = it.site ?: "",
                size = it.size ?: 0,
                name = it.name ?: "",
                id = it.id ?: "",
                type = it.type ?: "",
                key = it.key ?: ""
            )
        }
    }
}