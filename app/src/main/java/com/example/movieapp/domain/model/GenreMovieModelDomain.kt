package com.example.movieapp.domain.model

data class GenreMovieModelDomain(
    val name: String = "",
    val id: Int = 0
)