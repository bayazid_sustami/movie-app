package com.example.movieapp.domain.model

data class DetailMovieModelDomain(
    val id: Int,
    val video: Boolean,
    val title: String,
    val genres: List<GenreMovieModelDomain> = emptyList(),
    val popularity: Double,
    val voteCount: Int,
    val overview: String,
    val posterPath: String,
    val voteAverage: Double,
    val reviews: ReviewsMovieModelDomain,
    val videos: List<MovieVideoModelDomain>
)
