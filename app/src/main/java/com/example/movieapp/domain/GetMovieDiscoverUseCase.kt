package com.example.movieapp.domain

import com.example.movieapp.data.repository.discover.DiscoverMovieRepository
import com.example.movieapp.domain.mapper.HomeMapper
import com.example.movieapp.domain.model.DiscoverMovieModelDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetMovieDiscoverUseCase @Inject constructor(
    private val discoverMovieRepository: DiscoverMovieRepository,
    private val homeMapper: HomeMapper,
) {

    operator fun invoke(genreId: Int, page:Int) : Flow<DiscoverMovieModelDomain>{
        return discoverMovieRepository.getDiscoverMovieByGenre(genreId, page).map {
            homeMapper.mapDiscoverResponseToDomain(it)
        }
    }
}