package com.example.movieapp.domain.model

import java.text.SimpleDateFormat
import java.util.Locale

data class ReviewsMovieModelDomain(
    val id: Int,
    val page: Int,
    val totalPages: Int,
    val results: List<ReviewItem> = emptyList(),
    val totalResults: Int
) {
    data class ReviewItem(
        val author: String,
        val createdAt: String,
        val id: String,
        val content: String,
        val url: String
    ) {
        fun formattedCreatedAt(): String {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.US)
            val outputFormat = SimpleDateFormat("dd MMM yyyy", Locale.US)

            val date = inputFormat.parse(createdAt)
            return date?.let { outputFormat.format(it) } ?: ""
        }
    }
}