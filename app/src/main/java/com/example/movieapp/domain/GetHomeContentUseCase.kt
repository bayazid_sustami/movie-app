package com.example.movieapp.domain

import com.example.movieapp.data.repository.discover.DiscoverMovieRepository
import com.example.movieapp.data.repository.genre.GenreMovieRepository
import com.example.movieapp.domain.mapper.HomeMapper
import com.example.movieapp.domain.model.DiscoverMovieModelDomain
import com.example.movieapp.domain.model.GenreMovieModelDomain
import com.example.movieapp.domain.model.HomeModelDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetHomeContentUseCase @Inject constructor(
    private val discoverMovieRepository: DiscoverMovieRepository,
    private val genreMovieRepository: GenreMovieRepository,
    private val homeMapper: HomeMapper,
) {

    operator fun invoke() : Flow<HomeModelDomain<List<GenreMovieModelDomain>, DiscoverMovieModelDomain>> {
        return genreMovieRepository.getMovieGenres().flatMapConcat { genre ->
            val firstGenre = genre.genres.firstOrNull()?.id ?: 0
            return@flatMapConcat discoverMovieRepository
                .getDiscoverMovieByGenre(firstGenre, 1)
                .map { discover ->
                    HomeModelDomain(
                        genres = homeMapper.mapGenreResponseToDomain(genre),
                        movies = homeMapper.mapDiscoverResponseToDomain(discover)
                    )
                }
        }
    }


}