package com.example.movieapp.domain

import com.example.movieapp.data.repository.movie.MovieRepository
import com.example.movieapp.domain.mapper.DetailMapper
import com.example.movieapp.domain.model.ReviewsMovieModelDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetReviewMovieUseCase @Inject constructor(
    private val repository: MovieRepository,
    private val mapper: DetailMapper,
) {

    operator fun invoke(movieId: Int, page: Int) : Flow<ReviewsMovieModelDomain> {
        return repository.getMovieReviews(movieId, page).map {
            mapper.mapReviewsToDomain(it)
        }
    }
}