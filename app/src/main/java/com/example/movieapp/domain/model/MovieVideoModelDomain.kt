package com.example.movieapp.domain.model

data class MovieVideoModelDomain(
    val site: String,
    val size: Int,
    val name: String,
    val id: String,
    val type: String,
    val key: String,
)