package com.example.movieapp.domain

import com.example.movieapp.data.repository.movie.MovieRepository
import com.example.movieapp.domain.mapper.DetailMapper
import com.example.movieapp.domain.model.DetailMovieModelDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.zip
import javax.inject.Inject

class GetDetailMovieUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    private val mapper: DetailMapper,
) {

    operator fun invoke(movieId: Int) : Flow<DetailMovieModelDomain> {
        return combine(
            movieRepository.getDetailMovie(movieId),
            movieRepository.getMovieReviews(movieId, 1),
            movieRepository.getMovieVideos(movieId)
        ){movie, review, videos ->
            mapper.mapResponseDetailToDomain(movie, review, videos)
        }
    }

}