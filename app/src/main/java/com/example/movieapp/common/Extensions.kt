package com.example.movieapp.common

import androidx.compose.runtime.saveable.listSaver
import com.example.movieapp.presentation.uimodel.ChipGenresUIModel

val GenreSaver = listSaver<ChipGenresUIModel, Any>(
    save = { listOf(it.id, it.name, it.isSelected) },
    restore = { ChipGenresUIModel(
        id = it[0] as Int,
        name = it[1] as String,
        isSelected = it[2] as Boolean
    ) }
)
