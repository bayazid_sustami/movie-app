package com.example.movieapp.presentation.ui.screen

import android.content.Intent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.example.movieapp.presentation.activity.YoutubePlayerActivity
import com.example.movieapp.presentation.ui.components.CardReviewerHorizontal
import com.example.movieapp.presentation.ui.components.DetailHeader
import com.example.movieapp.presentation.ui.components.ErrorPage
import com.example.movieapp.presentation.ui.components.ShimmerView
import com.example.movieapp.presentation.ui.theme.MovieAppTheme
import com.example.movieapp.presentation.uimodel.DetailMovieUIModel
import com.example.movieapp.presentation.uimodel.MovieVideoUIModel
import com.example.movieapp.presentation.uimodel.ReviewerUIModel
import com.example.movieapp.presentation.uimodel.UIState
import com.example.movieapp.presentation.viewmodel.DetailMovieViewModel

@Composable
fun DetailScreen(
    modifier: Modifier = Modifier,
    viwModel: DetailMovieViewModel = hiltViewModel(),
    movieId: Int,
    onNavigateBack: () -> Unit,
    onSeeAllReviewerClicked: (Int) -> Unit
) {

    val context = LocalContext.current

    viwModel.movie.collectAsState().value.let { state ->
        when(state) {
            is UIState.Loading -> {
                DetailLoading()
            }
            is UIState.Success -> {
                DetailScreenContent(
                    modifier = modifier,
                    onNavigateBack = onNavigateBack,
                    data = state.data,
                    onSeeAllReviewerClicked = {
                        onSeeAllReviewerClicked.invoke(movieId)
                    },
                    onTrailerClicked = {
                        val intent = Intent(context, YoutubePlayerActivity::class.java).also { intent ->
                            intent.putExtra(YoutubePlayerActivity.KEY_EXTRA, it.key)
                        }
                        context.startActivity(intent)
                    }
                )
            }
            is UIState.Error -> {
                ErrorPage()
            }
        }
    }

    LaunchedEffect(key1 = Unit){
        viwModel.getDetailMovie(movieId)
    }
}

@Composable
fun DetailScreenContent(
    modifier: Modifier = Modifier,
    onNavigateBack: () -> Unit,
    data: DetailMovieUIModel,
    onSeeAllReviewerClicked: () -> Unit,
    onTrailerClicked: (MovieVideoUIModel) -> Unit,
) {
    Column(
        modifier = modifier
            .fillMaxSize(),
    ) {
        DetailHeader(
            title = "Detail Movie",
            onNavigateBack = onNavigateBack
        )
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .weight(1f)
        ) {
            MoviePoster(
                moviePoster = data.getFullPath(),
                videoData = data.video,
                onTrailerClicked = onTrailerClicked
            )
            MovieTitle(
                movieTitle = data.title,
                totalFavorite = data.getVoteCount()
            )
            MovieOverview(
                movieOverview = data.overview
            )
            if (data.reviews.isNotEmpty()) {
                MovieReviews(
                    items = data.reviews,
                    onSeeAllReviewerClicked = onSeeAllReviewerClicked
                )
            }
        }
    }
}

@Composable
fun MoviePoster(
    modifier: Modifier = Modifier,
    moviePoster: String,
    videoData: MovieVideoUIModel?,
    onTrailerClicked: (MovieVideoUIModel) -> Unit,
){
    Card(
        modifier = modifier
            .padding(start = 16.dp, end = 16.dp)
            .fillMaxWidth()
    ) {
        Box {
            AsyncImage(
                model = moviePoster,
                contentDescription = null,
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .height(570.dp)
            )
            if (videoData != null) {
                Card(
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.onPrimary
                    ),
                    modifier = Modifier
                        .padding(16.dp)
                        .align(Alignment.BottomEnd)
                        .clickable {
                            onTrailerClicked.invoke(videoData)
                        }
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .padding(vertical = 8.dp, horizontal = 12.dp)
                    ) {
                        Text(
                            text = "Trailer",
                            style = MaterialTheme.typography.labelLarge,
                            fontWeight = FontWeight.Bold,
                        )
                        Spacer(Modifier.padding(4.dp))
                        Icon(
                            imageVector = Icons.Filled.PlayArrow,
                            contentDescription = null
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun MovieTitle(
    modifier: Modifier = Modifier,
    movieTitle: String,
    totalFavorite: String,
) {
    Row(
        modifier = modifier
            .padding(16.dp)
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = movieTitle,
            style = MaterialTheme.typography.titleLarge,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.primary,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .weight(12f)
        )
        Spacer(Modifier.padding(4.dp))
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .weight(2f)
        ) {
            Text(
                text = totalFavorite,
                style = MaterialTheme.typography.labelLarge,
                color = MaterialTheme.colorScheme.primary,
            )
            Spacer(Modifier.padding(2.dp))
            Icon(
                imageVector = Icons.Filled.Favorite,
                contentDescription = null,
                modifier = Modifier
                    .size(24.dp),
                tint = Color.Red
            )
        }
    }
}

@Composable
fun MovieOverview(
    modifier: Modifier = Modifier,
    movieOverview: String
) {
    Column(
        modifier = modifier
            .padding(start = 16.dp, end = 16.dp)
    ) {
        Text(
            text = "Overview",
            style = MaterialTheme.typography.titleMedium,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.primary
        )
        Spacer(Modifier.padding(4.dp))
        Text(
            text = movieOverview,
            style = MaterialTheme.typography.bodyMedium,
            textAlign = TextAlign.Justify,
        )
    }
}

@Composable
fun MovieReviews(
    modifier: Modifier = Modifier,
    items: List<ReviewerUIModel>,
    onSeeAllReviewerClicked: () -> Unit
) {
    Column(
        modifier = modifier
            .padding(16.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = "Reviews",
                style = MaterialTheme.typography.titleMedium,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.primary
            )

            Text(
                text = "See All",
                style = MaterialTheme.typography.labelMedium,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier
                    .clickable { onSeeAllReviewerClicked.invoke() }
            )
        }
        Spacer(Modifier.padding(4.dp))
        LazyRow {
            items(items, key = {it.id}){
                CardReviewerHorizontal(data = it)
                Spacer(Modifier.padding(8.dp))
            }
        }
    }
}

@Composable
fun DetailLoading(
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .padding(16.dp)
    ) {
        ShimmerView(
            modifier = Modifier
                .fillMaxWidth()
                .height(570.dp)
                .clip(RoundedCornerShape(8.dp))
        )
        Spacer(Modifier.padding(8.dp))
        Row(
            modifier = modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            ShimmerView(
                modifier = Modifier
                    .size(125.dp,24.dp)
                    .clip(RoundedCornerShape(12.dp))
            )
            ShimmerView(
                modifier = Modifier
                    .size(50.dp,14.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
        }
        Spacer(Modifier.padding(8.dp))
        ShimmerView(
            modifier = Modifier
                .size(70.dp,14.dp)
                .clip(RoundedCornerShape(8.dp))
        )
        Spacer(Modifier.padding(6.dp))
        ShimmerView(
            modifier = Modifier
                .fillMaxWidth()
                .height(10.dp)
                .clip(RoundedCornerShape(8.dp))
        )
        Spacer(Modifier.padding(2.dp))
        ShimmerView(
            modifier = Modifier
                .fillMaxWidth()
                .height(10.dp)
                .clip(RoundedCornerShape(8.dp))
        )
        Spacer(Modifier.padding(2.dp))
        ShimmerView(
            modifier = Modifier
                .size(256.dp, 10.dp)
                .clip(RoundedCornerShape(8.dp))
        )
        Spacer(Modifier.padding(8.dp))
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            ShimmerView(
                modifier = Modifier
                    .size(70.dp,14.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
            ShimmerView(
                modifier = Modifier
                    .size(40.dp,12.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
        }
        Spacer(Modifier.padding(6.dp))
        LazyRow {
            items(5){
                ShimmerView(
                    modifier = Modifier
                        .size(125.dp, 50.dp)
                        .clip(RoundedCornerShape(8.dp))
                )
                Spacer(Modifier.padding(4.dp))
            }
        }
    }
}

@Composable
@Preview(showSystemUi = true)
fun DetailScreenPreview(){
    MovieAppTheme {
        DetailLoading()
    }
}