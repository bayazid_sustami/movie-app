package com.example.movieapp.presentation.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SuggestionChip
import androidx.compose.material3.SuggestionChipDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.movieapp.presentation.ui.theme.MovieAppTheme
import com.example.movieapp.presentation.uimodel.ChipGenresUIModel

@Composable
fun ChipGroup(
    modifier: Modifier = Modifier,
    items: List<ChipGenresUIModel>,
    onSelectedChanged:(ChipGenresUIModel) -> Unit
){
    Column(
        modifier = modifier
    ) {
        LazyRow(
            modifier = Modifier
                .padding(16.dp)
                .wrapContentSize(),
            horizontalArrangement = Arrangement.spacedBy(14.dp)
        ) {
            items(items){
                SuggestionChip(
                    onClick = {
                        onSelectedChanged.invoke(it)
                    },
                    label = {
                        Text(
                            text = it.name,
                            color = if (it.isSelected) Color.White else Color.Black
                        )
                    },
                    colors = if (it.isSelected) SuggestionChipDefaults.suggestionChipColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                    ) else SuggestionChipDefaults.suggestionChipColors(
                        containerColor = MaterialTheme.colorScheme.secondary
                    )
                )
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun ChipGroupPreview(){
    MovieAppTheme {
        ChipGroup(
            items = listOf(
                ChipGenresUIModel(1, "Action", false),
                ChipGenresUIModel(2, "Adventure", true),
                ChipGenresUIModel(3, "Animation", false),
                ChipGenresUIModel(4, "Drama", false),
            ),
            onSelectedChanged = { },
        )
    }
}