package com.example.movieapp.presentation.ui.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.repeatOnLifecycle
import com.example.movieapp.presentation.ui.components.CardPoster
import com.example.movieapp.presentation.ui.components.ChipGroup
import com.example.movieapp.presentation.ui.components.ErrorPage
import com.example.movieapp.presentation.ui.components.ShimmerView
import com.example.movieapp.presentation.ui.theme.MovieAppTheme
import com.example.movieapp.presentation.uimodel.CardMovieUIModel
import com.example.movieapp.presentation.uimodel.ChipGenresUIModel
import com.example.movieapp.presentation.uimodel.UIState
import com.example.movieapp.presentation.viewmodel.HomeViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@Composable
fun HomeScreen(
    modifier: Modifier = Modifier,
    viewModel: HomeViewModel = hiltViewModel(),
    onItemClicked: (CardMovieUIModel) -> Unit,
) {
    var isLoadingChips by remember { mutableStateOf(false) }
    var isLoadingCards by remember { mutableStateOf(false) }
    var isLoadMore by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }


    val lifecycle = LocalLifecycleOwner.current.lifecycle

    var genreId by rememberSaveable { mutableIntStateOf(0) }

    if (isLoadingChips) {
        HomeLoading()
    } else {
        if(isError) {
            ErrorPage()
        } else {
            HomeScreenContent(
                modifier = modifier,
                itemsChip = viewModel.dataGenre.collectAsState().value,
                itemsMovie = viewModel.dataMovie.collectAsState().value,
                onGenreClicked = {
                    genreId = it.id
                    viewModel.getMovieByGenre(genreId, 1)
                },
                onUpdatePage = {
                    viewModel.loadMoreMovies(genreId, it)
                },
                onItemClicked = onItemClicked,
                isLoadingMovie = isLoadingCards
            )
        }
    }

    LaunchedEffect(key1 = Unit) {
        lifecycle.repeatOnLifecycle(state = Lifecycle.State.STARTED) {
            launch {
                viewModel.movies.collectLatest { state ->
                    when(state) {
                        is UIState.Loading -> {
                            isLoadingCards = true
                            isError = false
                        }
                        is UIState.Success -> {
                            isLoadingCards = false
                            isError = false
                        }
                        is UIState.Error -> {
                            isLoadingCards = false
                            isError = true
                        }
                    }
                }
            }

            launch {
                viewModel.genres.collectLatest { state ->
                    when(state) {
                        is UIState.Loading -> {
                            isLoadingChips = true
                            isError = false
                        }
                        is UIState.Success -> {
                            isLoadingChips = false
                            isError = false
                            genreId = state.data.firstOrNull()?.id ?: 0
                        }
                        is UIState.Error -> {
                            isLoadingChips = false
                            isError = true
                        }
                    }
                }
            }

            launch {
                viewModel.moreMovies.collectLatest { state ->
                    when(state) {
                        is UIState.Loading -> {
                            isError = false
                            isLoadMore = true
                        }
                        is UIState.Success -> {
                            isLoadMore = false
                            isError = false
                        }
                        is UIState.Error -> {
                            isError = true
                            isLoadMore = false
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun HomeScreenContent(
    modifier: Modifier = Modifier,
    itemsChip: List<ChipGenresUIModel>,
    itemsMovie: List<CardMovieUIModel>,
    onGenreClicked: (ChipGenresUIModel) -> Unit,
    onUpdatePage: (Int) -> Unit,
    onItemClicked: (CardMovieUIModel) -> Unit,
    isLoadingMovie: Boolean
){
    var selectedId by remember { mutableIntStateOf(itemsChip.firstOrNull()?.id ?: 0) }

    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.SpaceAround
    ) {
        ChipGroup(
            modifier = Modifier,
            items = itemsChip,
            onSelectedChanged = {
                selectedId = it.id
                onGenreClicked.invoke(it)
            },
        )
        if (isLoadingMovie) {
            MovieCardLoading()
        } else {
            ImagesMovie(
                itemsMovie = itemsMovie.distinctBy { it.id },
                onUpdatePage = onUpdatePage,
                onItemClicked = onItemClicked,
            )
        }
    }
}

@Composable
fun ImagesMovie(
    modifier: Modifier = Modifier,
    itemsMovie: List<CardMovieUIModel>,
    onUpdatePage: (Int) -> Unit,
    onItemClicked: (CardMovieUIModel) -> Unit,
) {

    val listState = rememberLazyGridState()

    LazyVerticalGrid(
        modifier = modifier,
        columns = GridCells.Fixed(3),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(16.dp),
        state = listState
    ) {
        itemsIndexed(itemsMovie){ index, item ->
            CardPoster(movieData = item, onItemCLicked = onItemClicked)
            if (index  == itemsMovie.size - 1) {
                onUpdatePage.invoke(item.pageInfo.currentPage + 1)
            }
        }
    }
}

@Composable
fun HomeLoading(
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
    ) {
        LazyRow(
            modifier = Modifier
                .padding(16.dp)
        ) {
            items(4){
                ShimmerView(
                    modifier = Modifier
                        .size(96.dp, 32.dp)
                        .clip(RoundedCornerShape(8.dp))
                )
                Spacer(Modifier.padding(4.dp))
            }
        }
        MovieCardLoading()
    }
}

@Composable
fun MovieCardLoading(
    modifier: Modifier = Modifier
) {
    LazyVerticalGrid(
        modifier = modifier,
        columns = GridCells.Fixed(3),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(16.dp),
    ){
        items(12){
            ShimmerView(
                modifier = Modifier
                    .height(200.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
        }
    }
}

@Preview(
    showBackground = true,
    showSystemUi = true
)
@Composable
fun HomeScreenPreview(){
    MovieAppTheme {
        HomeLoading()
    }
}