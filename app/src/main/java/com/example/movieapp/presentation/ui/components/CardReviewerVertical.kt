package com.example.movieapp.presentation.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.movieapp.presentation.ui.theme.MovieAppTheme
import com.example.movieapp.presentation.uimodel.ReviewerUIModel

@Composable
fun CardReviewerVertical(
    modifier: Modifier = Modifier,
    data: ReviewerUIModel
) {
    var isExpand by remember { mutableStateOf(false) }

    Card(
        modifier = modifier
            .padding(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier
                .padding(8.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = data.author,
                    style = MaterialTheme.typography.labelLarge,
                )
                Text(
                    text = data.date,
                    style = MaterialTheme.typography.labelSmall,
                )
            }
            Spacer(Modifier.padding(4.dp))
            Text(
                text = data.content,
                style = MaterialTheme.typography.labelMedium,
                modifier = Modifier
                    .clickable {
                        isExpand = !isExpand
                    },
                maxLines = if (isExpand) Int.MAX_VALUE else 3,
                overflow = if (isExpand) TextOverflow.Clip else TextOverflow.Ellipsis,
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun CardReviewerVerticalPreview(){
    MovieAppTheme {
        CardReviewerVertical(data = ReviewerUIModel(
            id = "1",
            author = "bay",
            content = "khfdkaskf sdk  fkdjsa ajfhkj jfdskaj fkldkldf jfksdf fjdassafdasfjfhkasdh",
            date = "12 Des 2023",
        ))
    }
}