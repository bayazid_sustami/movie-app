package com.example.movieapp.presentation.uimodel

data class ReviewerUIModel(
    val id: String,
    val author: String,
    val content: String,
    val date: String,
    val pageInfo: ReviewPage = ReviewPage(0, 0, 0)
) {
    data class ReviewPage(
        val currentPage:Int,
        val totalPage:Int,
        val totalResult: Int
    )
}
