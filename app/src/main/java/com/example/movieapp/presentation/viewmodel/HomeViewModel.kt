package com.example.movieapp.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.common.coroutine.qualifiers.MainDispatcher
import com.example.movieapp.domain.GetHomeContentUseCase
import com.example.movieapp.domain.GetMovieDiscoverUseCase
import com.example.movieapp.domain.model.DiscoverMovieModelDomain
import com.example.movieapp.domain.model.GenreMovieModelDomain
import com.example.movieapp.presentation.uimodel.CardMovieUIModel
import com.example.movieapp.presentation.uimodel.ChipGenresUIModel
import com.example.movieapp.presentation.uimodel.MovieVideoUIModel
import com.example.movieapp.presentation.uimodel.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeContentUseCase: GetHomeContentUseCase,
    private val movieDiscoverUseCase: GetMovieDiscoverUseCase,
    @MainDispatcher
    private val dispatcher: CoroutineDispatcher,
) : ViewModel() {

    private val _genres = MutableSharedFlow<UIState<List<ChipGenresUIModel>>>()
    val genres: SharedFlow<UIState<List<ChipGenresUIModel>>> get() = _genres

    private val _movies = MutableSharedFlow<UIState<List<CardMovieUIModel>>>()
    val movies: SharedFlow<UIState<List<CardMovieUIModel>>> get() = _movies

    private val _moreMovies = MutableSharedFlow<UIState<List<CardMovieUIModel>>>()
    val moreMovies: SharedFlow<UIState<List<CardMovieUIModel>>> get() = _moreMovies

    private val _dataGenre = MutableStateFlow<List<ChipGenresUIModel>>(emptyList())
    val dataGenre: StateFlow<List<ChipGenresUIModel>> get() = _dataGenre

    private val _dataMovie = MutableStateFlow<List<CardMovieUIModel>>(emptyList())
    val dataMovie: StateFlow<List<CardMovieUIModel>> get() = _dataMovie

    private val genreItems = mutableListOf<ChipGenresUIModel>()

    private fun setSelectedId(id:Int) {
        viewModelScope.launch(dispatcher){
            val newList = genreItems.map { it.copy(isSelected = it.id == id) }
            genreItems.clear()
            genreItems.addAll(newList)

            _dataGenre.value = newList
            _genres.emit(UIState.Success(genreItems))
        }
    }

    init {
        getHomeContent()
    }

    private fun getHomeContent() {
        viewModelScope.launch(dispatcher){
            homeContentUseCase.invoke()
                .onStart {
                    _movies.emit(UIState.Loading)
                    _genres.emit(UIState.Loading)
                }.catch {
                    if (it is HttpException) {
                        _genres.emit(UIState.Error(it.message()))
                        _movies.emit(UIState.Error(it.message()))
                    }
                }.collectLatest {
                    val items = genreToUI(it.genres)
                    addToItemsChip(items)
                    setSelectedId(items.firstOrNull()?.id ?: 0)

                    val movie = movieToUI(it.movies)
                    _dataMovie.value = movie
                    _movies.emit(UIState.Success(movie))
                }
        }
    }

    fun getMovieByGenre(genreId: Int, page:Int) {
        setSelectedId(genreId)
        viewModelScope.launch(dispatcher){
            movieDiscoverUseCase.invoke(genreId, page)
                .onStart {
                    _movies.emit(UIState.Loading)
                }.catch {
                    if (it is HttpException) {
                        _movies.emit(UIState.Error(it.message()))
                    }
                }.collectLatest {
                    val movie = movieToUI(it)
                    _dataMovie.value = movie
                    _movies.emit(UIState.Success(movie))
                }
        }
    }

    fun loadMoreMovies(genreId: Int, page:Int) {
        viewModelScope.launch(dispatcher){
            movieDiscoverUseCase.invoke(genreId, page)
                .onStart {
                    _moreMovies.emit(UIState.Loading)
                }.catch {
                    if (it is HttpException) {
                        _moreMovies.emit(UIState.Error(it.message()))
                    }
                }.collectLatest {
                    val movie = movieToUI(it)
                    _dataMovie.value += movie
                    _moreMovies.emit(UIState.Success(movie))
                }
        }
    }

    private fun addToItemsChip(items: List<ChipGenresUIModel>) {
        genreItems.clear()
        genreItems.addAll(items)
    }

    private fun genreToUI(genres: List<GenreMovieModelDomain>): List<ChipGenresUIModel> {
        return genres.map {
            ChipGenresUIModel(
                id = it.id,
                name = it.name,
            )
        }
    }

    private fun movieToUI(movie: DiscoverMovieModelDomain): List<CardMovieUIModel> {
        return movie.results.map {
            CardMovieUIModel(
                id = it.id,
                imagePoster = it.posterPath,
                title = it.title,
                voteAverage = it.voteAverage,
                voteTotal = it.voteCount,
                pageInfo = CardMovieUIModel.MoviePage(
                    currentPage = movie.page,
                    totalPage = movie.totalPages,
                    totalResult = movie.totalResults
                )
            )
        }
    }


}
