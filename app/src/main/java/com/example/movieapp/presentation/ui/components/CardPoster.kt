package com.example.movieapp.presentation.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.movieapp.presentation.ui.theme.MovieAppTheme
import com.example.movieapp.presentation.uimodel.CardMovieUIModel

@Composable
fun CardPoster(
    modifier: Modifier = Modifier,
    movieData: CardMovieUIModel,
    onItemCLicked: (CardMovieUIModel) -> Unit,
) {
    Card(
        modifier = modifier
            .clickable {
                onItemCLicked.invoke(movieData)
            },
        shape = RoundedCornerShape(8.dp),
    ) {
        Column {
            AsyncImage(
                model = movieData.getFullPath(),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
            )
            Text(
                modifier = Modifier
                    .padding(8.dp),
                text = movieData.title,
                style = MaterialTheme.typography.bodySmall,
                color = MaterialTheme.colorScheme.primary,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun CardPosterPreview() {
    MovieAppTheme {
        CardPoster(
            movieData = CardMovieUIModel(
                id = 100,
                imagePoster = "https://image.tmdb.org/t/p/w154/9tGyxJcPUQclngSCcT18oEs80Yn.jpg",
                title = "Wolf Warrior",
                voteAverage = 6.3,
                voteTotal = 217,
            ),
            onItemCLicked = {}
        )
    }
}