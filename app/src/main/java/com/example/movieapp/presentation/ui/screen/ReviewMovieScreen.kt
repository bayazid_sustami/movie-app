package com.example.movieapp.presentation.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.repeatOnLifecycle
import com.example.movieapp.presentation.ui.components.CardReviewerVertical
import com.example.movieapp.presentation.ui.components.DetailHeader
import com.example.movieapp.presentation.ui.components.ErrorPage
import com.example.movieapp.presentation.ui.components.ShimmerView
import com.example.movieapp.presentation.ui.theme.MovieAppTheme
import com.example.movieapp.presentation.uimodel.ReviewerUIModel
import com.example.movieapp.presentation.uimodel.UIState
import com.example.movieapp.presentation.viewmodel.DetailMovieViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@Composable
fun ReviewMovieScreen(
    modifier: Modifier = Modifier,
    viewModel: DetailMovieViewModel = hiltViewModel(),
    onNavigateBack: () ->  Unit,
    movieId: Int,
) {
    val lifecycle = LocalLifecycleOwner.current.lifecycle

    var itemReview by remember { mutableStateOf(listOf<ReviewerUIModel>()) }
    var isLoading by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }

    if (isLoading) {
        ReviewsLoading()
    } else {
        if (isError) {
            ErrorPage()
        } else {
            ReviewMovieContent(
                modifier = modifier,
                onNavigateBack = onNavigateBack,
                itemsReview = itemReview,
                onUpdatePage = { page ->
                    viewModel.getMoreMovieReview(movieId, page)
                }
            )
        }
    }

    LaunchedEffect(key1 = Unit){
        viewModel.getMovieReview(movieId)

        lifecycle.repeatOnLifecycle(state = Lifecycle.State.STARTED) {
            launch {
                viewModel.reviews.collectLatest { state ->
                    when(state) {
                        is UIState.Loading -> {
                            isLoading = true
                            isError = false
                        }
                        is UIState.Success -> {
                            isLoading = false
                            isError = false
                            itemReview = state.data
                        }
                        is UIState.Error -> {
                            isLoading = false
                            isError = true
                        }
                    }
                }
            }

            launch {
                viewModel.moreReviews.collectLatest { state ->
                    when(state) {
                        is UIState.Loading -> {}
                        is UIState.Success -> {
                            itemReview += state.data
                        }
                        is UIState.Error -> {}
                    }
                }
            }
        }
    }
}

@Composable
fun ReviewMovieContent(
    modifier: Modifier = Modifier,
    onNavigateBack: () ->  Unit,
    itemsReview: List<ReviewerUIModel>,
    onUpdatePage: (Int) -> Unit,
) {
    Column(
        modifier = modifier
    ) {
        DetailHeader(
            title = "Reviews",
            onNavigateBack = onNavigateBack
        )
        LazyColumn(
            contentPadding = PaddingValues(vertical = 8.dp)
        ) {
            itemsIndexed(itemsReview){index, item ->
                CardReviewerVertical(data = item)
                if (index  == itemsReview.size - 1) {
                    onUpdatePage.invoke(item.pageInfo.currentPage + 1)
                }
            }
        }
    }
}

@Composable
fun ReviewsLoading(
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(vertical = 8.dp)
    ) {
        items(10){
            ShimmerView(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp)
                    .padding(
                        start = 16.dp,
                        end = 16.dp,
                        top = 8.dp,
                        bottom = 8.dp,
                    )
                    .clip(RoundedCornerShape(8.dp))
            )
            Spacer(Modifier.padding(4.dp))
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun ReviewMovieScreenPreview(){
    MovieAppTheme {
        ReviewsLoading()
    }
}