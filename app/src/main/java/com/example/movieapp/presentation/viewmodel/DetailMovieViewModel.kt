package com.example.movieapp.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.common.coroutine.qualifiers.MainDispatcher
import com.example.movieapp.domain.GetDetailMovieUseCase
import com.example.movieapp.domain.GetReviewMovieUseCase
import com.example.movieapp.domain.model.DetailMovieModelDomain
import com.example.movieapp.domain.model.GenreMovieModelDomain
import com.example.movieapp.domain.model.MovieVideoModelDomain
import com.example.movieapp.domain.model.ReviewsMovieModelDomain
import com.example.movieapp.presentation.uimodel.ChipGenresUIModel
import com.example.movieapp.presentation.uimodel.DetailMovieUIModel
import com.example.movieapp.presentation.uimodel.MovieVideoUIModel
import com.example.movieapp.presentation.uimodel.ReviewerUIModel
import com.example.movieapp.presentation.uimodel.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val getDetailMovieUseCase: GetDetailMovieUseCase,
    private val getReviewsUseCase: GetReviewMovieUseCase,
    @MainDispatcher
    private val dispatcher: CoroutineDispatcher
): ViewModel() {

    private val _movie = MutableStateFlow<UIState<DetailMovieUIModel>>(UIState.Loading)
    val movie: StateFlow<UIState<DetailMovieUIModel>> get() = _movie

    private val _reviews = MutableSharedFlow<UIState<List<ReviewerUIModel>>>()
    val reviews: SharedFlow<UIState<List<ReviewerUIModel>>> get() = _reviews

    private val _moreReviews = MutableSharedFlow<UIState<List<ReviewerUIModel>>>()
    val moreReviews: SharedFlow<UIState<List<ReviewerUIModel>>> get() = _moreReviews

    fun getDetailMovie(movieId: Int) {
        viewModelScope.launch(dispatcher){
            getDetailMovieUseCase.invoke(movieId)
                .onStart {
                    _movie.value = UIState.Loading
                }.catch {
                    if (it is HttpException) {
                        _movie.value = UIState.Error(it.message())
                    }
                }.collectLatest {
                    _movie.value = UIState.Success(detailMovieToUI(it))
                }
        }
    }

    private fun detailMovieToUI(data: DetailMovieModelDomain): DetailMovieUIModel {
        return DetailMovieUIModel(
            id = data.id,
            video = detailVideoToUI(data.videos),
            title = data.title,
            genres = data.genres.map { genreToUI(it) },
            popularity = data.popularity,
            voteCount = data.voteCount,
            overview = data.overview,
            posterPath = data.posterPath,
            voteAverage = data.voteAverage,
            reviews = reviewsToUi(data.reviews)
        )
    }

    private fun detailVideoToUI(videos: List<MovieVideoModelDomain>) : MovieVideoUIModel? {
        return videos.filter { it.site.lowercase() == "youtube" }
            .filter { it.type.lowercase() == "trailer" }
            .map {
                MovieVideoUIModel(
                    site = it.site,
                    name = it.name,
                    type = it.type,
                    key = it.key
                )
            }.firstOrNull()
    }

    private fun genreToUI(genre : GenreMovieModelDomain): ChipGenresUIModel {
        return ChipGenresUIModel(
            id = genre.id,
            name = genre.name,
        )
    }

    private fun reviewsToUi(review: ReviewsMovieModelDomain): List<ReviewerUIModel> {
        return review.results.map {
            ReviewerUIModel(
                id = it.id,
                author = it.author,
                content = it.content,
                date = it.formattedCreatedAt(),
                pageInfo = ReviewerUIModel.ReviewPage(
                    totalResult = review.totalResults,
                    currentPage = review.page,
                    totalPage = review.totalPages
                )
            )
        }
    }

    fun getMovieReview(movieId: Int) {
        viewModelScope.launch(dispatcher){
            getReviewsUseCase.invoke(movieId, 1)
                .onStart {
                    _reviews.emit(UIState.Loading)
                }.catch {
                    if (it is HttpException){
                        _reviews.emit(UIState.Error(it.message()))
                    }
                }.collectLatest {
                    _reviews.emit(UIState.Success(reviewsToUi(it)))
                }
        }
    }

    fun getMoreMovieReview(movieId: Int, page: Int) {
        viewModelScope.launch(dispatcher){
            getReviewsUseCase.invoke(movieId, page)
                .onStart {
                    _moreReviews.emit(UIState.Loading)
                }.catch {
                    if (it is HttpException){
                        _moreReviews.emit(UIState.Error(it.message()))
                    }
                }.collectLatest {
                    _moreReviews.emit(UIState.Success(reviewsToUi(it)))
                }
        }
    }
}