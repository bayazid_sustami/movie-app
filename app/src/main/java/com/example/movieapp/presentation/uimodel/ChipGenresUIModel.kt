package com.example.movieapp.presentation.uimodel

data class ChipGenresUIModel(
    val id: Int,
    val name: String,
    val isSelected: Boolean = false
)