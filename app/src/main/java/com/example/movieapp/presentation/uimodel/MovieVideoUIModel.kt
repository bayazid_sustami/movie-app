package com.example.movieapp.presentation.uimodel

data class MovieVideoUIModel(
    val site: String,
    val name: String,
    val type: String,
    val key: String,
)