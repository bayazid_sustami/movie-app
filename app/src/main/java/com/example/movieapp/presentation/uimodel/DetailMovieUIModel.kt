package com.example.movieapp.presentation.uimodel

data class DetailMovieUIModel(
    val id: Int,
    val video: MovieVideoUIModel?,
    val title: String,
    val genres: List<ChipGenresUIModel> = emptyList(),
    val popularity: Double,
    val voteCount: Int,
    val overview: String,
    val posterPath: String,
    val voteAverage: Double,
    val reviews: List<ReviewerUIModel>,
) {
    fun getFullPath(): String {
        return "https://image.tmdb.org/t/p/w500$posterPath"
    }

    fun getVoteCount(): String {
        return when {
            voteCount in 1000..9999 -> "${voteCount / 1000}.${(voteCount % 1000) / 100}k"
            voteCount >= 10000 -> "${voteCount / 1000}k"
            else -> voteCount.toString()
        }
    }
}