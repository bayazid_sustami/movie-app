package com.example.movieapp.presentation.uimodel

data class CardMovieUIModel(
    val id:Int,
    val imagePoster:String,
    val title:String,
    val voteAverage: Double,
    val voteTotal:Int,
    val pageInfo: MoviePage = MoviePage(0, 0, 0)
) {
    data class MoviePage(
        val currentPage:Int,
        val totalPage:Int,
        val totalResult: Int
    )

    fun getFullPath(): String {
        return "https://image.tmdb.org/t/p/w154$imagePoster"
    }
}
