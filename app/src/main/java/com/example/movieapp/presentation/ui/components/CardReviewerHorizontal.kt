package com.example.movieapp.presentation.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.movieapp.presentation.ui.theme.MovieAppTheme
import com.example.movieapp.presentation.uimodel.ReviewerUIModel

@Composable
fun CardReviewerHorizontal(
    modifier: Modifier = Modifier,
    data: ReviewerUIModel
) {
    Card(
        modifier = modifier
            .width(256.dp)
    ) {
        Column(
            modifier = Modifier
                .padding(8.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = data.author,
                    style = MaterialTheme.typography.labelLarge,
                )
                Text(
                    text = data.date,
                    style = MaterialTheme.typography.labelSmall,
                )
            }
            Spacer(Modifier.padding(4.dp))
            Text(
                text = data.content,
                style = MaterialTheme.typography.labelMedium,
                modifier = Modifier,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun CardReviewerHorizontalPreview(){
    MovieAppTheme {
        CardReviewerHorizontal(data = ReviewerUIModel(
            id = "1",
            author = "bay",
            content = "khfdkaskf sdk  fkdjsa ajfhkj jfdskajfkldkldf jfksdf fjdasf fdsajfhkasdh jfdshajf hfdjsahf hfdjshfajfdsa jfshaj jfhsjdhfjfshdjffhdsjh",
            date = "12 Des 2023",
        ))
    }
}