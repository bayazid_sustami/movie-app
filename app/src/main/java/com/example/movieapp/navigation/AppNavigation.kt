package com.example.movieapp.navigation

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.movieapp.presentation.ui.screen.DetailScreen
import com.example.movieapp.presentation.ui.screen.HomeScreen
import com.example.movieapp.presentation.ui.screen.ReviewMovieScreen

@Composable
fun AppNavigation(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
) {
    Scaffold(
        modifier = modifier
    ) {
        NavHost(
            navController = navController,
            startDestination = AppRouter.HomePage.route,
            modifier = Modifier.padding(it)
        ) {
            composable(AppRouter.HomePage.route){
                HomeScreen(
                    onItemClicked = {data ->
                        navController.navigate(AppRouter.DetailPage.createRoute(data.id))
                    }
                )
            }
            composable(
                route = AppRouter.DetailPage.route,
                arguments = listOf(
                    navArgument(
                        name = AppRouter.KEY_MOVIE_ID,
                    ){
                        type = NavType.IntType
                    }
                )
            ){ stackEntry ->
                val movieId = stackEntry.arguments?.getInt(AppRouter.KEY_MOVIE_ID)
                DetailScreen(
                    movieId = movieId ?: 0,
                    onNavigateBack = {
                        navController.navigateUp()
                    },
                    onSeeAllReviewerClicked = { id ->
                        navController.navigate(AppRouter.ReviewerPage.createRoute(id))
                    }
                )
            }
            composable(
                route = AppRouter.ReviewerPage.route,
                arguments = listOf(
                    navArgument(
                        name = AppRouter.KEY_MOVIE_ID,
                    ){
                        type = NavType.IntType
                    }
                )
            ){ stackEntry ->
                val movieId = stackEntry.arguments?.getInt(AppRouter.KEY_MOVIE_ID)
                ReviewMovieScreen(
                    movieId = movieId ?: 0,
                    onNavigateBack = {
                        navController.navigateUp()
                    },
                )
            }
        }
    }
}