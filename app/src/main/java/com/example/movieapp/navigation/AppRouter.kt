package com.example.movieapp.navigation

sealed class AppRouter(val route: String) {

    data object HomePage : AppRouter(HOMEPAGE_ROUTE)
    data object DetailPage : AppRouter("$DETAIL_PAGE_ROUTE/{$KEY_MOVIE_ID}") {
        override fun <T> createRoute(args: T): String = "$DETAIL_PAGE_ROUTE/$args"
    }
    data object ReviewerPage : AppRouter("$REVIEWER_PAGE_ROUTE/{$KEY_MOVIE_ID}") {
        override fun <T> createRoute(args: T) = "$REVIEWER_PAGE_ROUTE/$args"
    }

    open fun <T> createRoute(args: T) : String {
        return ""
    }

    companion object {
        const val HOMEPAGE_ROUTE = "homepage"
        const val DETAIL_PAGE_ROUTE = "detail-page"
        const val REVIEWER_PAGE_ROUTE = "reviewer-page"

        const val KEY_MOVIE_ID = "movie_id"
    }
}