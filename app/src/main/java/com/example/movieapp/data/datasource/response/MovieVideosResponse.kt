package com.example.movieapp.data.datasource.response

import com.google.gson.annotations.SerializedName

data class MovieVideosResponse(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("results")
	val results: List<VideoItem> = emptyList()
){
	data class VideoItem(

		@field:SerializedName("site")
		val site: String? = null,

		@field:SerializedName("size")
		val size: Int? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("id")
		val id: String? = null,

		@field:SerializedName("type")
		val type: String? = null,

		@field:SerializedName("key")
		val key: String? = null
	)
}
