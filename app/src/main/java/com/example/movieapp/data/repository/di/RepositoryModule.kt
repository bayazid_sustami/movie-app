package com.example.movieapp.data.repository.di

import com.example.movieapp.data.repository.discover.DiscoverMovieRepository
import com.example.movieapp.data.repository.discover.DiscoverMovieRepositoryImpl
import com.example.movieapp.data.repository.genre.GenreMovieRepository
import com.example.movieapp.data.repository.genre.GenreMovieRepositoryImpl
import com.example.movieapp.data.repository.movie.MovieRepository
import com.example.movieapp.data.repository.movie.MovieRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindGenreMovieRepository(repository: GenreMovieRepositoryImpl): GenreMovieRepository

    @Binds
    abstract fun bindDiscoverMovieRepository(repository: DiscoverMovieRepositoryImpl): DiscoverMovieRepository

    @Binds
    abstract fun bindMovieRepository(repository: MovieRepositoryImpl): MovieRepository
}