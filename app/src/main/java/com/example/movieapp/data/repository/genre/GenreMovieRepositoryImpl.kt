package com.example.movieapp.data.repository.genre

import com.example.movieapp.common.coroutine.qualifiers.IODispatcher
import com.example.movieapp.data.datasource.network.ApiResult
import com.example.movieapp.data.datasource.network.ApiService
import com.example.movieapp.data.datasource.response.GenresResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.HttpException
import javax.inject.Inject

class GenreMovieRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IODispatcher
    private val dispatcher: CoroutineDispatcher,
): GenreMovieRepository {

    override fun getMovieGenres(): Flow<GenresResponse> {
        return flow {
            val movieGenre = apiService.getMovieGenre()
            emit(movieGenre)
        }.flowOn(dispatcher)
    }
}