package com.example.movieapp.data.repository.discover

import com.example.movieapp.common.coroutine.qualifiers.IODispatcher
import com.example.movieapp.data.datasource.network.ApiResult
import com.example.movieapp.data.datasource.network.ApiService
import com.example.movieapp.data.datasource.response.DiscoverMovieResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.HttpException
import javax.inject.Inject

class DiscoverMovieRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IODispatcher
    private val dispatcher: CoroutineDispatcher,
) : DiscoverMovieRepository {

    override fun getDiscoverMovieByGenre(id: Int, page: Int): Flow<DiscoverMovieResponse> {
        return flow {
            val movieGenre = apiService.getDiscoverMoviesByGenre(
                genreId = id,
                page = page
            )
            emit(movieGenre)
        }.flowOn(dispatcher)
    }
}