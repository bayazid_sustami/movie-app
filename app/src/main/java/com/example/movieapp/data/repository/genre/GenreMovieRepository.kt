package com.example.movieapp.data.repository.genre

import com.example.movieapp.data.datasource.response.GenresResponse
import kotlinx.coroutines.flow.Flow

interface GenreMovieRepository {
    fun getMovieGenres(): Flow<GenresResponse>
}