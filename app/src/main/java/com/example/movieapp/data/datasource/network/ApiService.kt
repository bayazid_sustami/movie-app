package com.example.movieapp.data.datasource.network

import com.example.movieapp.data.datasource.response.DetailMovieResponse
import com.example.movieapp.data.datasource.response.DiscoverMovieResponse
import com.example.movieapp.data.datasource.response.GenresResponse
import com.example.movieapp.data.datasource.response.MovieVideosResponse
import com.example.movieapp.data.datasource.response.ReviewsMovieResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("genre/movie/list")
    suspend fun getMovieGenre() : GenresResponse

    @GET("discover/movie")
    suspend fun getDiscoverMoviesByGenre(
        @Query("with_genres") genreId: Int,
        @Query("page") page:Int,
        @Query("sort_by") sortBy: String = "popularity.desc",
        @Query("include_adult") includeAdult: Boolean = false,
        @Query("include_video") includeVideo: Boolean = false,
    ): DiscoverMovieResponse

    @GET("movie/{movie_id}")
    suspend fun getDetailMovie(@Path("movie_id") movieId: Int): DetailMovieResponse

    @GET("movie/{movie_id}/reviews")
    suspend fun getReviews(
        @Path("movie_id") movieId: Int,
        @Query("page") page:Int,
    ): ReviewsMovieResponse

    @GET("movie/{movie_id}/videos")
    suspend fun getMovieVideos(@Path("movie_id") movieId: Int): MovieVideosResponse
}