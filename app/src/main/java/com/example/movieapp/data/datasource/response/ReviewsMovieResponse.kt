package com.example.movieapp.data.datasource.response

import com.google.gson.annotations.SerializedName

data class ReviewsMovieResponse(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("page")
	val page: Int? = null,

	@field:SerializedName("total_pages")
	val totalPages: Int? = null,

	@field:SerializedName("results")
	val results: List<ReviewItem> = emptyList(),

	@field:SerializedName("total_results")
	val totalResults: Int? = null
) {
	data class ReviewItem(

		@field:SerializedName("updated_at")
		val updatedAt: String? = null,

		@field:SerializedName("author")
		val author: String? = null,

		@field:SerializedName("created_at")
		val createdAt: String? = null,

		@field:SerializedName("id")
		val id: String? = null,

		@field:SerializedName("content")
		val content: String? = null,

		@field:SerializedName("url")
		val url: String? = null
	)
}
