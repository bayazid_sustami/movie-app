package com.example.movieapp.data.repository.discover

import com.example.movieapp.data.datasource.network.ApiResult
import com.example.movieapp.data.datasource.response.DiscoverMovieResponse
import kotlinx.coroutines.flow.Flow

interface DiscoverMovieRepository {
    fun getDiscoverMovieByGenre(id:Int, page:Int): Flow<DiscoverMovieResponse>
}