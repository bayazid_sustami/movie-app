package com.example.movieapp.data.repository.movie

import com.example.movieapp.common.coroutine.qualifiers.IODispatcher
import com.example.movieapp.data.datasource.network.ApiService
import com.example.movieapp.data.datasource.response.DetailMovieResponse
import com.example.movieapp.data.datasource.response.MovieVideosResponse
import com.example.movieapp.data.datasource.response.ReviewsMovieResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IODispatcher
    private val dispatcher: CoroutineDispatcher
) : MovieRepository {

    override fun getDetailMovie(movieId: Int): Flow<DetailMovieResponse> {
        return flow {
            val result = apiService.getDetailMovie(movieId)
            emit(result)
        }.flowOn(dispatcher)
    }

    override fun getMovieReviews(movieId: Int, page: Int): Flow<ReviewsMovieResponse> {
        return flow {
            val result = apiService.getReviews(movieId, page)
            emit(result)
        }.flowOn(dispatcher)
    }

    override fun getMovieVideos(movieId: Int): Flow<MovieVideosResponse> {
        return flow {
            val result = apiService.getMovieVideos(movieId)
            emit(result)
        }.flowOn(dispatcher)
    }


}