package com.example.movieapp.data.repository.movie

import com.example.movieapp.data.datasource.response.DetailMovieResponse
import com.example.movieapp.data.datasource.response.MovieVideosResponse
import com.example.movieapp.data.datasource.response.ReviewsMovieResponse
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    fun getDetailMovie(movieId: Int): Flow<DetailMovieResponse>
    fun getMovieReviews(movieId: Int, page: Int): Flow<ReviewsMovieResponse>
    fun getMovieVideos(movieId: Int): Flow<MovieVideosResponse>
}